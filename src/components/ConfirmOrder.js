import React from "react";
import PropTypes from "prop-types";
import { motion } from "framer-motion";
import "../styles/confirmorder.scss";
import { FaThumbsUp } from "react-icons/fa";
import { Link } from "react-router-dom";
import Translate from "../locale/Translate";

const ConfirmOrder = ({ setConfirmModal }) => {
	return (
		<div className="confirm-container">
			<motion.div
				initial={{ opacity: 0, scale: 0.7 }}
				whileInView={{ opacity: 1, scale: 1 }}
				className="confirm"
			>
				<span className="confirm-heading">{Translate("cm.title")}</span>
				<motion.span
					className="confirm-icon"
					animate={{ scale: 1.1, transition: { yoyo: Infinity } }}
				>
					<FaThumbsUp />
				</motion.span>
				<span className="confirm-desc">{Translate("cm.msg")}</span>
				<div className="confirm-actions">
					<button className="cancel" onClick={() => setConfirmModal(false)}>
						{Translate("cm.cancel")}
					</button>
					<Link to="/orders" className="submit">
						{Translate("cm.placeOrder")}
					</Link>
				</div>
			</motion.div>
		</div>
	);
};

export default ConfirmOrder;

ConfirmOrder.propTypes = {
	confirmModal: PropTypes.any,
	setConfirmModal: PropTypes.any,
};
