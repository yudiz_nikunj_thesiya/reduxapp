import React, { useEffect, useState } from "react";
import "../styles/cart.scss";
import PropTypes from "prop-types";
import { motion } from "framer-motion";
import { addProductToCart, updateQuantity } from "../app/action-creators";
import { useDispatch, useSelector } from "react-redux";
import Translate from "../locale/Translate";

const Cart = ({ setCartModal, modalProduct }) => {
	const [quantity, setQuantity] = useState(1);
	const [productVariant, setProductVariant] = useState({});
	const [productExtras, setProductExtras] = useState([]);
	const [productExtrasPrice, setProductExtrasPrice] = useState(0);
	const [category, setCategory] = useState("");
	const getCategories = useSelector((state) => state.categories?.categories);
	const getCart = useSelector((state) => state.cart.cartProduct);

	const [cartProduct, setCartProduct] = useState({
		name: modalProduct.name,
		totalPrice: modalProduct.price,
		quantity: quantity,
		variant: "",
		extras: [],
		category,
	});

	const dispatch = useDispatch();
	useEffect(() => {
		if (modalProduct.variants !== undefined) {
			setProductVariant(modalProduct.variants[0]);
		}

		const category = getCategories.find(
			(cate) => cate.id == modalProduct.parentId
		);
		const mainCate = getCategories.find((cate) => cate.id == category.parent);
		setCategory(mainCate.name);
	}, []);

	useEffect(() => {
		if (
			modalProduct.variants !== undefined &&
			modalProduct.extras !== undefined
		) {
			setCartProduct({
				...cartProduct,
				quantity,
				category,
				extras: productExtras,
				variant: productVariant.name,
				totalPrice: quantity * (productExtrasPrice + productVariant.price),
			});
		} else if (
			modalProduct.variants === undefined &&
			modalProduct.extras === undefined
		) {
			setCartProduct({
				...cartProduct,
				quantity,
				category,
				totalPrice: quantity * modalProduct.price,
			});
		} else if (modalProduct.variants === undefined) {
			setCartProduct({
				...cartProduct,
				quantity,
				category,
				extras: productExtras,
				totalPrice: quantity * (productExtrasPrice + modalProduct.price),
			});
		} else if (modalProduct.extras === undefined) {
			setCartProduct({
				...cartProduct,
				quantity,
				category,
				variant: productVariant.name,
				totalPrice: quantity * productVariant.price,
			});
		}
	}, [quantity, productVariant, productExtras, productExtrasPrice]);

	const handleOrder = () => {
		const matchedProduct = getCart?.filter(
			(item) =>
				item?.name == cartProduct?.name &&
				JSON.stringify(item?.extras) == JSON.stringify(cartProduct?.extras) &&
				item?.variant == cartProduct?.variant
		);

		if (matchedProduct?.length !== 0) {
			matchedProduct?.map((item) => {
				if (
					item?.name === cartProduct.name &&
					item?.variant === cartProduct.variant &&
					JSON.stringify(item?.extras) === JSON.stringify(cartProduct.extras)
				) {
					dispatch(
						updateQuantity(
							item?.name,
							cartProduct?.quantity,
							cartProduct?.totalPrice,
							cartProduct?.variant,
							cartProduct?.extras
						)
					);
				}
			});
		} else {
			dispatch(
				addProductToCart({
					// id: new Date().valueOf(),
					...cartProduct,
					quantity,
					category,
				})
			);
		}

		setCartModal(false);
	};

	const handleOnChangeOnExtras = (e) => {
		const { value, checked } = e.target;

		const onChangeProduct = modalProduct.extras.find(
			(extra) => extra.name == value
		);

		if (checked) {
			setProductExtras([...productExtras, onChangeProduct]);
			setProductExtrasPrice(productExtrasPrice + onChangeProduct.price);
		} else {
			setProductExtras([
				...productExtras.filter((extra) => extra.name !== value),
			]);
			setProductExtrasPrice(productExtrasPrice - onChangeProduct.price);
		}
	};

	const handleOnChangeOnVariant = (e) => {
		const mainVariant = modalProduct.variants.find(
			(variant) => variant.name == e.target.value
		);
		setProductVariant(mainVariant);
	};

	return (
		<motion.div
			initial={{ opacity: 0, y: "100vh" }}
			whileInView={{ opacity: 1, y: 0 }}
			className="cart-container"
		>
			<div className="transparent" onClick={() => setCartModal(false)}></div>
			<div className="cart">
				<div className="cart-header">
					<span className="lbl">{modalProduct?.name}</span>
					<span className="percent">Lager 5.2%</span>
					<div className="divider"></div>
				</div>
				{modalProduct?.variants ? (
					<div className="cart-items">
						<span className="lbl">{Translate("pm.size")}</span>
						{modalProduct?.variants.map((variant, index) => (
							<div key={index} className="variant__box">
								<input
									type="radio"
									id={variant.name}
									name="variant"
									value={variant.name}
									checked={productVariant == variant}
									onChange={handleOnChangeOnVariant}
								/>
								<label className="variant-name" htmlFor={variant.name}>
									{variant.name}
								</label>
								<label className="variant-price" htmlFor={variant.name}>
									${variant.price}
								</label>
							</div>
						))}

						<div className="divider"></div>
					</div>
				) : (
					<div className="cart-items">
						<span className="lbl">{Translate("pm.options")}</span>

						<div className="item">
							<span>{modalProduct?.name}</span>
							<span>${modalProduct?.price}</span>
						</div>

						<div className="divider"></div>
					</div>
				)}

				{modalProduct.extras && (
					<div className="cart-options">
						<span className="lbl">{Translate("pm.options")}</span>
						{modalProduct.extras.map((extra, index) => (
							<div className="item" key={index}>
								<span>
									{extra.name} (+ ${extra.price})
								</span>
								<div className="item-checkbox">
									<input
										type="checkbox"
										className="item-checkbox__input"
										id={extra.name}
										name={extra.name}
										value={extra.name}
										onChange={handleOnChangeOnExtras}
									/>
									<label
										className="item-checkbox__label item-checkbox__label--orange"
										htmlFor={extra.name}
									></label>
								</div>
							</div>
						))}

						<div className="divider"></div>
					</div>
				)}
				<div className="cart-counter">
					<button onClick={() => quantity > 1 && setQuantity(quantity - 1)}>
						-
					</button>
					<span>{quantity}</span>
					<button onClick={() => setQuantity(quantity + 1)}>+</button>
				</div>
				<button className="btn" onClick={handleOrder}>
					{Translate("btn.addtoorder")}
				</button>
			</div>
		</motion.div>
	);
};

export default Cart;

Cart.propTypes = {
	cartModal: PropTypes.any,
	setCartModal: PropTypes.any,
	modalProduct: PropTypes.any,
};
