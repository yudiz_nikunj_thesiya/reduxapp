import React from "react";
import "../styles/itemcard.scss";
import PropTypes from "prop-types";

const ItemCard = ({ heading, desc, price, currency, onClick }) => {
	return (
		<div className="itemcard" onClick={onClick}>
			<div className="itemcard-left">
				<span className="heading">{heading}</span>
				<span className="desc">{desc}</span>
			</div>
			<div className="itemcard-right">
				{currency} {price}
			</div>
		</div>
	);
};

export default ItemCard;

ItemCard.propTypes = {
	heading: PropTypes.string,
	desc: PropTypes.string,
	price: PropTypes.number,
	currency: PropTypes.string,
	onClick: PropTypes.any,
};
