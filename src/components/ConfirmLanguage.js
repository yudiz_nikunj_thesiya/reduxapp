import React from "react";
import { motion } from "framer-motion";
import PropTypes from "prop-types";
import "../styles/confirmlanguage.scss";
import { LOCALES } from "../locale";
import { useDispatch, useSelector } from "react-redux";
import { setLanguage } from "../app/action-creators";
import Translate from "../locale/Translate";

const ConfirmLanguage = ({ setLanguageModal }) => {
	const dispatch = useDispatch();
	const getLanguage = useSelector((state) => state.language);

	const setLang = (lang) => {
		setLanguageModal(false);
		dispatch(setLanguage(lang));
	};

	return (
		<div className="lang-container" onClick={() => setLanguageModal(false)}>
			<motion.div
				initial={{ opacity: 0, scale: 0.7 }}
				whileInView={{ opacity: 1, scale: 1 }}
				className="lang"
			>
				<span className="lang-heading">{Translate("selectLang")}</span>
				<div className="lang-actions">
					<span
						className={getLanguage == LOCALES.ENGLISH ? "btn-active" : "btn"}
						onClick={() => setLang(LOCALES.ENGLISH)}
					>
						ENGLISH
					</span>
					<span
						className={getLanguage == LOCALES.GERMAN ? "btn-active" : "btn"}
						onClick={() => setLang(LOCALES.GERMAN)}
					>
						GERMAN
					</span>
					<span
						className={getLanguage == LOCALES.FRENCH ? "btn-active" : "btn"}
						onClick={() => setLang(LOCALES.FRENCH)}
					>
						FRENCH
					</span>
				</div>
			</motion.div>
		</div>
	);
};

export default ConfirmLanguage;

ConfirmLanguage.propTypes = {
	setLanguageModal: PropTypes.any,
};
