// import { useSelector } from "react-redux";
import { Routes, Route } from "react-router-dom";
// import Cart from "./components/Cart";
import Checkout from "./pages/Checkout";
import Home from "./pages/Home";
import Order from "./pages/Order";
import "./styles/app.scss";
import { I18nProvider } from "./locale";
import { useSelector } from "react-redux";

function App() {
	const language = useSelector((state) => state.language);

	return (
		<div className="app">
			<I18nProvider locale={language}>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/checkout" element={<Checkout />} />
					<Route path="/orders" element={<Order />} />
				</Routes>
			</I18nProvider>
		</div>
	);
}

export default App;
