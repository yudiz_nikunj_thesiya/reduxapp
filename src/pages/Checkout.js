import React, { useEffect, useState } from "react";
import { FiChevronLeft } from "react-icons/fi";
import { BsThreeDots } from "react-icons/bs";
import "../styles/checkout.scss";
import ConfirmOrder from "../components/ConfirmOrder";
import Cart from "../components/Cart";
import { motion } from "framer-motion";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import Translate from "../locale/Translate";

const Checkout = () => {
	const [cartModal, setCartModal] = useState(false);
	const [confirmModal, setConfirmModal] = useState(false);
	const [categories, setCategories] = useState([]);
	const navigate = useNavigate();

	const [cartTotal, setCartTotal] = useState(0);
	const getCart = useSelector((state) => state.cart);

	const getCategories = useSelector((state) => state.categories?.categories);
	useEffect(() => {
		getCart.cartProduct.length === 0 && navigate("/");
	}, []);
	useEffect(() => {
		setCategories(getCategories.filter((category) => category.parent == null));
	}, []);

	useEffect(() => {
		const total = getCart.cartProduct.reduce((acu, curr) => {
			return acu + parseFloat(curr?.totalPrice);
		}, 0);
		setCartTotal(total);
	}, [getCart]);

	const [fileredProducts, setFilteredProducts] = useState([]);
	useEffect(() => {
		const filterProdArr = [];

		categories.map(
			(category) =>
				(filterProdArr[category.name] = {
					products: getCart.cartProduct.filter(
						(product) => product?.category == category?.name
					),
				})
		);
		setFilteredProducts(filterProdArr);
	}, [categories]);

	return (
		<>
			<motion.div
				initial={{ opacity: 0, scale: 0.7 }}
				animate={{ opacity: 1, scale: 1 }}
				className="checkout"
			>
				<div className="checkout-navigation">
					<span className="icon" onClick={() => navigate(-1)}>
						<FiChevronLeft />
					</span>
					<span className="label">{Translate("checkout")}</span>
					<span className="icon">
						<BsThreeDots />
					</span>
				</div>
				<div className="checkout-header">
					<span className="heading">{Translate("main.heading")}</span>
					<span className="address">
						{Translate("address.street")} <br />
						{Translate("address.city")}
					</span>
				</div>
				{Object.keys(fileredProducts).map((category, index) => (
					<div className="checkout-items" key={index}>
						{fileredProducts[category].products?.length !== 0 && (
							<span className="lbl">
								{category} (
								{fileredProducts[category].products?.reduce((acc, cur) => {
									return acc + cur?.quantity;
								}, 0)}
								)
							</span>
						)}
						<div className="items">
							{fileredProducts[category].products?.map((product, index) => (
								<div className="item" key={index}>
									<div className="item-desc">
										<span className="quantity">
											{product?.quantity} x {product?.name}
										</span>
										<span className="options">
											<span>{product?.variant !== "" && product?.variant}</span>
											{product.extras.map((extra, index) => (
												<span key={index}>
													{product?.variant !== ""
														? `, ${extra?.name}`
														: extra?.name}
												</span>
											))}
										</span>
									</div>
									<div className="item-price">
										${product.totalPrice.toFixed(2)}
									</div>
								</div>
							))}
						</div>
					</div>
				))}
				<div className="divider"></div>
				<div className="checkout-add-notes">
					<span>{Translate("cart.notes")}</span>
					<textarea className="txtarea" rows="8"></textarea>
					<div className="divider"></div>
				</div>
				<div className="checkout-tbl-num">
					<span className="lbl">{Translate("cart.table")}</span>
					<span className="num">32</span>
				</div>
				<div className="checkout-confirm" onClick={() => setConfirmModal(true)}>
					<span className="lbl">{Translate("btn.confirm")}</span>
					<span className="num">
						${cartTotal} / {getCart.cartProduct.length} {Translate("btn.item")}
					</span>
				</div>
				{confirmModal === true && (
					<ConfirmOrder
						confirmModal={confirmModal}
						setConfirmModal={setConfirmModal}
					/>
				)}
				{cartModal === true && (
					<Cart cartModal={cartModal} setCartModal={setCartModal} />
				)}
			</motion.div>
		</>
	);
};

export default Checkout;
