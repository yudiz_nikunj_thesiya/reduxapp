import React, { useEffect } from "react";
import { FiChevronLeft } from "react-icons/fi";
import { BsThreeDots } from "react-icons/bs";
import { motion } from "framer-motion";
import logo from "../Assets/logo.png";
import "../styles/order.scss";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Translate from "../locale/Translate";
import { clearCart } from "../app/action-creators";

const Order = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const getCart = useSelector((state) => state.cart);
	useEffect(() => {
		getCart.cartProduct.length === 0 && navigate("/");
	}, []);

	return (
		<motion.div
			className="order"
			initial={{ opacity: 0, scale: 0.7 }}
			animate={{ opacity: 1, scale: 1 }}
		>
			<div className="order-navigation">
				<span
					className="icon"
					onClick={() => {
						dispatch(clearCart());
						navigate(-2, { replace: true });
					}}
				>
					<FiChevronLeft />
				</span>
				<span className="label">{Translate("order.heading")}</span>
				<span className="icon">
					<BsThreeDots />
				</span>
			</div>
			<div className="order-header">
				<span className="heading">{Translate("main.heading")}</span>
				<span className="address">
					{Translate("address.street")} <br />
					{Translate("address.city")}
				</span>
			</div>

			<span>{Translate("order.confirm")}</span>

			<div className="order-items">
				{getCart.cartProduct.map((item, index) => (
					<div className="item" key={index}>
						<div className="lbl">{item.category}</div>
						<div className="item-name">
							<span>
								{item.name} x {item.quantity}
							</span>
							<img src={logo} alt="logo" />
						</div>
					</div>
				))}
			</div>
		</motion.div>
	);
};

export default Order;
