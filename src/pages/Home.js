import React, { useEffect, useState } from "react";
import "../styles/home.scss";
import ScrollContainer from "react-indiana-drag-scroll";
import ItemCard from "../components/ItemCard";
import { motion } from "framer-motion";
import Cart from "../components/Cart";
import { IoLanguageSharp } from "react-icons/io5";
import { useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import ConfirmLanguage from "../components/ConfirmLanguage";
import Translate from "../locale/Translate";

const Home = () => {
	const [categories, setCategories] = useState([]);
	const [subCategories, setSubCategories] = useState([]);
	const [cartModal, setCartModal] = useState(false);
	const [activeCategory, setActiveCategory] = useState({
		category: "",
		subcategory: "",
	});

	const [filteredProducts, setFilteredProducts] = useState([]);
	const [cartTotal, setCartTotal] = useState(0);
	const [languageModal, setLanguageModal] = useState(false);
	const [modalProduct, setModalProduct] = useState({});

	const navigate = useNavigate();

	const getCategories = useSelector((state) => state.categories?.categories);
	const getProducts = useSelector((state) => state.products?.products);
	const getCart = useSelector((state) => state.cart);
	const [searchParams] = useSearchParams();

	useEffect(() => {
		setCategories(getCategories?.filter((category) => category.parent == null));
	}, []);

	useEffect(() => {
		setActiveCategory((prev) => ({
			...prev,
			category: searchParams.get("category") || categories[0]?.id,
		}));

		setSubCategories(
			getCategories?.filter(
				(item) =>
					item.parent == (searchParams.get("category") || categories[0]?.id)
			)
		);
	}, [categories]);

	useEffect(() => {
		setFilteredProducts(
			getProducts?.filter(
				(product) =>
					product.parentId ==
					(searchParams.get("subcategory") || subCategories[0]?.id)
			)
		);
		setActiveCategory((prev) => ({
			...prev,
			subcategory: searchParams.get("subcategory") || subCategories[0]?.id,
		}));
	}, [subCategories]);

	const handleCategory = (id) => {
		setSubCategories(
			getCategories?.filter((category) => category.parent == id)
		);
		setActiveCategory((prev) => ({ ...prev, category: id }));
		navigate({ pathname: "", search: `?category=${id}` }, { replace: true });
	};

	const handleSubCategory = (id) => {
		setFilteredProducts(
			getProducts?.filter((product) => product.parentId == id)
		);
		setActiveCategory((prev) => ({ ...prev, subcategory: id }));
		navigate(
			{
				pathname: "",
				search: `category=${activeCategory.category}&subcategory=${id}`,
			},
			{ replace: true }
		);
	};

	useEffect(() => {
		const total = getCart?.cartProduct.reduce((acu, curr) => {
			return acu + parseFloat(curr?.totalPrice);
		}, 0);
		setCartTotal(total);
	}, [getCart]);

	return (
		<motion.div
			className="home"
			initial={{ opacity: 0, scale: 0.7 }}
			whileInView={{ opacity: 1, scale: 1 }}
			animate={{ opacity: 1, scale: 1 }}
		>
			<div className="home-header">
				<span className="heading">{Translate("main.heading")}</span>
				<span className="address">
					{Translate("address.street")}
					<br />
					{Translate("address.city")}
				</span>
			</div>

			<div className="home-categories">
				{categories?.map((category) => (
					<div
						key={category.id}
						className={
							(searchParams.get("category") || activeCategory.category) ==
							category.id
								? "category-active"
								: "category"
						}
						onClick={() => handleCategory(category.id)}
					>
						{category.name}
					</div>
				))}
			</div>

			<ScrollContainer className="home-subcategories">
				{subCategories?.map((subCategory) => (
					<span
						className={
							(searchParams.get("subcategory") || activeCategory.subcategory) ==
							subCategory.id
								? "subcategory-active"
								: "subcategory"
						}
						key={subCategory.id}
						onClick={() => handleSubCategory(subCategory.id, subCategory.name)}
					>
						{subCategory.name}
					</span>
				))}
			</ScrollContainer>
			<div className="home-itemcard-container">
				{filteredProducts?.length > 0 ? (
					filteredProducts?.map((product) => (
						<motion.div
							initial={{ opacity: 0, scale: 0.7 }}
							whileInView={{ opacity: 1, scale: 1 }}
							key={product?.id}
						>
							<ItemCard
								heading={product?.name}
								desc={product?.description}
								price={product?.price}
								currency="$"
								onClick={() => {
									setCartModal(true);
									setModalProduct(product);
								}}
							/>
						</motion.div>
					))
				) : (
					<motion.div
						initial={{ opacity: 0, scale: 0.7 }}
						animate={{ opacity: 1, scale: 1 }}
						className="product-not-found"
					>
						Product will Available Soon.
					</motion.div>
				)}
			</div>
			<div className="language" onClick={() => setLanguageModal(true)}>
				<IoLanguageSharp />
			</div>
			{getCart.cartProduct.length > 0 && (
				<div className="checkout-confirm" onClick={() => navigate("/checkout")}>
					<span className="lbl">{Translate("btn.viewCart")}</span>
					<span className="num">
						${cartTotal} / {getCart.cartProduct.length} {Translate("btn.item")}
					</span>
				</div>
			)}
			{languageModal === true && (
				<ConfirmLanguage
					languageModal={languageModal}
					setLanguageModal={setLanguageModal}
					modalProduct={modalProduct}
				/>
			)}
			{cartModal === true && (
				<Cart
					cartModal={cartModal}
					setCartModal={setCartModal}
					modalProduct={modalProduct}
				/>
			)}
		</motion.div>
	);
};

export default Home;
