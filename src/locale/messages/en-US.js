import { LOCALES } from "../locales";
const data = {
	[LOCALES.ENGLISH]: {
		selectLang: "Select Language",
		checkout: "Checkout",
		"main.heading": "Kings Arms Cardington",
		"address.street": "134 High Street, Kempston Bedford,",
		"address.city": "Bedfordshire, MK42 7BN",
		"btn.viewCart": "VIEW BASKET",
		"btn.confirm": "CONFIRM ORDER",
		"btn.addtoorder": "ADD TO ORDER",
		"pm.size": "Size",
		"pm.options": "Select Options",
		"title.checkout": "Checkout",
		"cart.notes": "Add Notes :",
		"cart.table": "Table Number",
		"cm.title": "Confirm Order",
		"cm.msg":
			"By placing this order you agree that you are present in Kings Arms and over 18 years old.",
		"cm.cancel": "CANCEL",
		"cm.placeOrder": "PLACE ORDER",
		"order.heading": "Orders",
		"order.confirm": "Confirmed Orders",
		"btn.item": "ITEMS",
	},
};
export default data;
