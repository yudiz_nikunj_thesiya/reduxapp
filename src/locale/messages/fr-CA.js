import { LOCALES } from "../locales";
const data = {
	[LOCALES.FRENCH]: {
		selectLang: "Choisir la langue",
		checkout: "Vérifier",
		"main.heading": "Armes des rois Cardington",
		"address.street": "134, rue High, Kempston Bedford,",
		"address.city": "Bedfordshire, MK42 7BN",
		"btn.viewCart": "VOIR LE PANIER",
		"btn.confirm": "CONFIRMER LA COMMANDE",
		"btn.addtoorder": "AJOUTER À LA COMMANDE",
		"pm.size": "Taille",
		"pm.options": "Sélectionnez les options",
		"title.checkout": "Vérifier",
		"cart.notes": "Ajouter des remarques :",
		"cart.table": "Numéro de tableau",
		"cm.title": "Confirmer la commande",
		"cm.msg":
			"En passant cette commande, vous reconnaissez que vous êtes présent à Kings Arms et que vous avez plus de 18 ans.",
		"cm.cancel": "CANCEL",
		"cm.placeOrder": "PASSER LA COMMANDE",
		"order.heading": "Ordres",
		"order.confirm": "Commandes confirmées",
		"btn.item": "ÉLÉMENTS",
	},
};
export default data;
