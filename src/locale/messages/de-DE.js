import { LOCALES } from "../locales";

const data = {
	[LOCALES.GERMAN]: {
		selectLang: "Sprache auswählen",
		checkout: "Kasse",
		"main.heading": "King's Arms Cardington",
		"address.street": "134 Hauptstraße, Kempston Bedford,",
		"address.city": "Bedfordshire, MK42 7BN",
		"btn.viewCart": "EINKAUFSWAGEN ANSEHEN",
		"btn.confirm": "BESTELLUNG BESTÄTIGEN",
		"btn.addtoorder": "ZUR BESTELLUNG HINZUFÜGEN",
		"pm.size": "Größe",
		"pm.options": "Optionen wählen",
		"title.checkout": "Kasse",
		"cart.notes": "Notizen hinzufügen :",
		"cart.table": "Tisch Nummer",
		"cm.title": "Bestellung bestätigen",
		"cm.msg":
			"Indem Sie diese Bestellung aufgeben, stimmen Sie zu, dass Sie in Kings Arms anwesend und über 18 Jahre alt sind.",
		"cm.cancel": "ABBRECHEN",
		"cm.placeOrder": "BESTELLUNG AUFGEBEN",
		"order.heading": "Aufträge",
		"order.confirm": "Bestätigte Bestellungen",
		"btn.item": "ARTIKEL",
	},
};
export default data;
