import React, { Fragment } from "react";
import { IntlProvider } from "react-intl";
import { LOCALES } from "./locales";
import messages from "./messages";
import PropTypes from "prop-types";

function Provide({ children, locale = LOCALES.ENGLISH }) {
	return (
		<IntlProvider
			locale={locale}
			textComponent={Fragment}
			messages={messages[locale]}
		>
			{children}
		</IntlProvider>
	);
}

export default Provide;

Provide.propTypes = {
	children: PropTypes.any,
	locale: PropTypes.any,
};
