export const setCategories = (category) => {
	return {
		type: "SET_CATEGORIES",
		payload: category,
	};
};

export const setProducts = (id) => {
	return {
		type: "SET_PRODUCTS",
		payload: id,
	};
};
export const setLanguage = (lang) => {
	return {
		type: "SET_LANGUAGE",
		payload: lang,
	};
};

export const addProductToCart = (product) => {
	return {
		type: "ADD_TO_CART",
		payload: product,
	};
};

export const updateQuantity = (name, quantity, price, variant, extras) => {
	return {
		type: "UPDATE_QUANTITY",
		payload: { name, quantity, price, variant, extras },
	};
};

export const setTotalCartPrice = (price) => {
	return {
		type: "TOTAL_CART_PRICE",
		payload: price,
	};
};

export const clearCart = () => {
	return {
		type: "CLEAR_CART",
	};
};
