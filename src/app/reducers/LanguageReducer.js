import { LOCALES } from "../../locale/locales";

const initialState = LOCALES.ENGLISH;

const languageReducer = (state = initialState, action) => {
	switch (action.type) {
		case "SET_LANGUAGE":
			return action.payload;

		default:
			return state;
	}
};

export default languageReducer;
