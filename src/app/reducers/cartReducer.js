const initialState = {
	cartProduct: [],
	totalCartPrice: 0,
};

const cartReducer = (state = initialState, action) => {
	switch (action.type) {
		case "ADD_TO_CART":
			return {
				...state,
				cartProduct: [action.payload, ...state.cartProduct],
			};

		case "UPDATE_QUANTITY":
			return {
				...state,
				cartProduct: state.cartProduct.map((prod) => {
					if (
						prod?.name == action.payload.name &&
						prod?.variant == action.payload.variant &&
						JSON.stringify(prod?.extras) ==
							JSON.stringify(action.payload.extras)
					) {
						return {
							...prod,
							quantity: prod?.quantity + action.payload?.quantity,
							totalPrice: prod?.totalPrice + action.payload?.price,
						};
					} else {
						return prod;
					}
				}),
			};
		case "TOTAL_CART_PRICE":
			return {
				...state,
				totalCartPrice: action.payload,
			};

		case "CLEAR_CART":
			return {
				cartProduct: [],
				totalCartPrice: 0,
			};
		default:
			return state;
	}
};

export default cartReducer;
