import categories from "../../data/Categories.json";

const initialState = {
	categories: categories.categories,
};

const categoriesReducer = (state = initialState, action) => {
	switch (action.type) {
		case "SET_CATEGORIES":
			return {
				...state,
				categories: [...state.categories, action.payload],
			};
		default:
			return state;
	}
};

export default categoriesReducer;
