import { combineReducers } from "redux";
import cartReducer from "./cartReducer";
import categoriesReducer from "./categoriesReducer";
import languageReducer from "./LanguageReducer";
import productsReducer from "./productsReducers";

const reducers = combineReducers({
	language: languageReducer,
	categories: categoriesReducer,
	products: productsReducer,
	cart: cartReducer,
});

export default reducers;
