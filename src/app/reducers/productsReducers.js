import products from "../../data/products.json";

const initialState = {
	products: products.products,
	cartProduct: [],
	totalCartPrice: 0,
};

const productsReducer = (state = initialState, action) => {
	switch (action.type) {
		case "SET_PRODUCTS":
			return {
				...state,
				products: [...state.products, action.payload],
			};
		case "ADD_TO_CART":
			return {
				...state,
				cartProduct: [action.payload, ...state.cartProduct],
			};

		case "TOTAL_CART_PRICE":
			return {
				...state,
				totalCartPrice: action.payload,
			};

		case "CLEAR_CART":
			return {
				...state,
				cartProduct: [],
				totalCartPrice: 0,
			};
		default:
			return state;
	}
};

export default productsReducer;
